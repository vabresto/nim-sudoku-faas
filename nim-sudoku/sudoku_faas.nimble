# Package

version       = "1.0.0"
author        = "Victor Brestoiu"
description   = "A sudoku solver Function-as-a-Service"
license       = "MIT"
srcDir        = "src"
bin           = @["main"]
binDir        = "bin"


# Dependencies

requires "nim >= 1.4.0"
