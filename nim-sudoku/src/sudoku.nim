import strutils
import options
import tables

const SudokuSize* = 9
const SudokuTotal = int((SudokuSize  * (SudokuSize + 1)) / 2)
type 
  GameBoard* = array[SudokuSize, array[SudokuSize, int]]

{.experimental: "strictFuncs".}

func numOrSpace(num: int, isExport: bool = false): string =
  if num > 0:
    $num
  elif isExport:
    "-"
  else:
    " "

iterator column(board: GameBoard, col: int): int {.noSideEffect.} = 
  for x in low(board) .. high(board):
    yield board[x][col]

iterator miniSquare(board: GameBoard, c_x: int, c_y: int): int {.noSideEffect.} =
  assert low(board) < c_x 
  assert c_x < high(board)
  assert low(board[c_x]) < c_y
  assert c_y < high(board[c_x])

  for x in -1 .. 1:
    for y in -1 .. 1:
      yield board[c_x + x][c_y + y]

func serialize*(board: GameBoard, isExport: bool = false): string =
  for x in low(board) .. high(board):
    for y in low(board[x]) .. high(board[x]):
      result.add(board[x][y].numOrSpace(isExport))
    result.add("\n")

proc display*(board: GameBoard) = 
  stdout.write board.serialize

proc deserialize*(stream: File): GameBoard = 
  const zeroOrd = ord('0')
  for x in 0 ..< SudokuSize:
    var row: array[SudokuSize, int] = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    for y in 0 ..< SudokuSize:
      let nextChar = stream.readChar()
      row[y] = if nextChar.isDigit: ord(nextChar) - zeroOrd else: 0
    discard stream.readChar # Read newline
    result[x] = deepCopy(row)

func deserialize*(stream: string): GameBoard =
  const zeroOrd = ord('0')
  var idx = 0
  for x in 0 ..< SudokuSize:
    var row: array[SudokuSize, int] = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    for y in 0 ..< SudokuSize:
      let nextChar = stream[idx]
      row[y] = if nextChar.isDigit: ord(nextChar) - zeroOrd else: 0
      idx += 1
    idx += 1 # Skip newline
    result[x] = deepCopy(row)

func editBoard*(board: GameBoard, x, y, val: int): GameBoard =
  for pos_x in low(board) .. high(board):
    for pos_y in low(board) .. high(board):
      if pos_x == x and pos_y == y:
        result[pos_x][pos_y] = val
      else:
        result[pos_x][pos_y] = board[pos_x][pos_y]

func verifyComplete*(board: GameBoard): bool = 
  # Verify each row
  for x in low(board) .. high(board):
    var s: set[int8] = {}
    var total = 0
    for y in low(board[x]) .. high(board[x]):
      s = s + {int8(board[x][y])}
      total += board[x][y]
    if len(s) < SudokuSize or total != SudokuTotal:
      return false
  
  # Verify each col
  for y in low(board) .. high(board):
    var s: set[int8] = {}
    var total = 0
    for val in board.column(y):
      s = s + {int8(val)}
      total += val
    if len(s) < SudokuSize or total != SudokuTotal:
      return false

  # Verify each mini square
  for x in countup(1, high(board), 3):
    for y in countup(1, high(board[x]), 3):
      var s: set[int8] = {}
      var total = 0
      for val in board.miniSquare(x, y):
        s = s + {int8(val)}
        total += val
      if len(s) < SudokuSize or total != SudokuTotal:
        return false

  return true

func partialVerify*(board: GameBoard): bool = 
  # Used to verify if a solution is possible given this state
  # For example, disallows duplicates (other than the digit 0)
  # Verify each row
  for x in low(board) .. high(board):
    var dict = initCountTable[int]()
    for y in low(board[x]) .. high(board[x]):
      dict.inc(board[x][y])
    for key, value in dict:
      if key != 0 and value > 1:
        return false
  
  # Verify each col
  for y in low(board) .. high(board):
    var dict = initCountTable[int]()
    for val in board.column(y):
      dict.inc(val)
    for key, value in dict:
      if key != 0 and value > 1:
        return false

  # Verify each mini square
  for x in countup(1, high(board), 3):
    for y in countup(1, high(board[x]), 3):
      var dict = initCountTable[int]()
      for val in board.miniSquare(x, y):
        dict.inc(val)
      for key, value in dict:
        if key != 0 and value > 1:
          return false

  return true

func findZero(board: GameBoard): Option[(int, int)] = 
  for x in low(board) .. high(board):
    for y in low(board) .. high(board):
      if board[x][y] == 0:
        let tup = (int(x), int(y))
        return some(tup)
  return none((int, int))

iterator branch(board: GameBoard): GameBoard {.noSideEffect.} =
  # Find the first 0
  let zeroes = board.findZero
  assert zeroes.isSome
  let (x, y) = zeroes.get
  for num in countup(1, SudokuSize):
    yield board.editBoard(x, y, num)
  

func solve*(board: GameBoard): Option[GameBoard] = 
  for candidate in board.branch:
    # Check if we're done
    if candidate.verifyComplete:
      return some(candidate)
    
    # Check if we should discard
    if not candidate.partialVerify:
      continue

    # Not done, but still ok state
    let sol = candidate.solve
    if sol.isSome:
      return sol
  return none(GameBoard)
