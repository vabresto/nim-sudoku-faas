import fixed_asynchttpserver, asyncdispatch
import strformat, strutils
import options
import re

import sudoku

func GameBoardToEditableHtml(board: GameBoard, editable: bool = true): string =
  let disabled = if not editable: "disabled" else: ""
  result &= """<form action="" method="post" id="sudoku">"""
  result &= """<table id="grid">"""

  var cnt = 0
  for x in low(board) .. high(board):
    result &= """<tr>"""
    for y in low(board[x]) .. high(board[x]):
      result &= fmt"""<td><input id="cell-{cnt}" name="cell-{cnt}"  type="text" value="{board[x][y]}" {disabled}></td>"""
      cnt += 1
    result &= """</tr>"""
  result &= """</table>"""
  result &= """</form>"""

func PostToGameBoard(post: string): GameBoard =
  result = GameBoard([
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
    ])
  
  for cell in post.split("&"):
    var values: array[2, string]
    var matched = find(cell, re"^cell-(\d{1,2})=(\d)$", values)
    if matched >= 0:
      let cell_id: int = parseInt(values[0])
      let cell_val: int = parseInt(values[1])

      let x = int(cell_id / 9)
      let y = cell_id mod 9
      
      if 0 <= x and x < SudokuSize and 0 <= y and y < SudokuSize:
        result = result.editBoard(x, y, cell_val)

var server = newAsyncHttpServer()
proc cb(req: Request) {.async.} =
  const get_head = staticRead("get_head.html")
  const get_foot = staticRead("get_foot.html")
  const solve_button = """<button class="solve" type="submit" form="sudoku">Solve</button><br>"""
  const reset_button = """<button class="solve" onclick="window.location = window.location.href;">Reset</button>"""

  # GET
  if req.reqMethod == HttpGet:
    let board = GameBoard([
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
    ])
    
    let board_html = GameBoardToEditableHtml(board)
    let final_body = get_head & board_html & solve_button & reset_button & get_foot
    await req.respond(Http200, final_body)

  # POST
  elif req.reqMethod == HttpPost:
    let board = PostToGameBoard(req.body)
    let maybe_solved = board.solve
    if maybe_solved.isSome:
      let solved = maybe_solved.get
      let board_html = GameBoardToEditableHtml(solved, editable=false)
      let final_body = get_head & board_html & solve_button & reset_button & get_foot
      await req.respond(Http200, final_body)
    else:
      let board_html = GameBoardToEditableHtml(board)
      let error = """<p class="error-message">The specified board has no possible solutions<p>"""
      let final_body = get_head & board_html & error & solve_button & reset_button & get_foot
      await req.respond(Http200, final_body)

  # Only handle GET or POST
  else:
    await req.respond(Http500, "Invalid request type, must be GET or POST")

waitFor server.serve(Port(8888), cb)